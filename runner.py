from os.path import join, dirname
from textx import metamodel_from_file
from textx.export import metamodel_export, model_export
from robot import Robot

def move_command_processor(move_cmd):
    """
    This is object processor for MoveCommand instances.
    It implements a default step of 1 in case not given
    in the program.
    """

    if move_cmd.steps == 0:
        move_cmd.steps = 1

def main(debug=False):

    this_folder = dirname(__file__)

    robot_mm = metamodel_from_file(join(this_folder, 'robot.tx'), debug=False)
    metamodel_export(robot_mm, join(this_folder, 'robot_meta.dot'))

    # Register object processor for MoveCommand
    robot_mm.register_obj_processors({'MoveCommand': move_command_processor})

    robot_model = robot_mm.model_from_file(join(this_folder, 'program.rbt'))
    model_export(robot_model, join(this_folder, 'program.dot'))

    robot = Robot()
    robot.interpret(robot_model)


if __name__ == "__main__":
    main()