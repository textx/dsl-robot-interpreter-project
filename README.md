http://textx.github.io/textX/3.0/tutorials/robot/

To run the robot, run `python runner.py`

runner.py is what will run the .rbt file

program.rbt is what will be interpreted.

robot.py is the interpreter for .rbt files

robot.tx is the meta-model for the DSL